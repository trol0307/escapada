<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Traveler extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'traveler';

    protected $primaryKey = 'id';
}
