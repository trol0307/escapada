<?php

namespace App\Console\Commands\PreBooking;

use Illuminate\Console\Command;
use App\Models\PreBooking as PreBooking;

class PreBookingList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prebooking:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Llista de prereserves';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $headers = ['id', 'cottage', 'data inici', 'data final', 'persones',
        'nom propietari', 'telefon propietari', 'nom traveler',
        'telefon traveler', 'tipus prebooking', 'punts', 'creat'];

        $preBookings = PreBooking::all();
        $rows = [];
        foreach($preBookings as $preBooking) {
          $row = [];
          $cottage = $preBooking->cottage;
          $owner = $cottage->owner;
          $traveler = $preBooking->traveler;
          $prebookingType = $preBooking->preBookingType;

          $row[] = $cottage->id;
          $row[] = $cottage->name;
          $row[] = $preBooking->start_date;
          $row[] = $preBooking->end_date;
          $row[] = $preBooking->people_number;
          $row[] = $owner->firstName . ' ' . $owner->secondName;
          $row[] = $owner->phone;
          $row[] = $traveler->firstName . ' ' . $traveler->lastName;
          $row[] = $traveler->phone;
          $row[] = $prebookingType->name;
          $row[] = $prebookingType->points;
          $row[] = $preBooking->created_at;
          $rows[] = $row;

        }

        $this->table($headers, $rows);
    }
}
