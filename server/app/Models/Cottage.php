<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Owner as Owner;

class Cottage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cottage';

    protected $primaryKey = 'id';

    /**
     * Get owner for this cottage
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(Owner::class, 'owner_id', 'id');
    }

}
