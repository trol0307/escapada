<?php

use Illuminate\Database\Seeder;
use App\Models\Owner as Owner;

class OwnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conn = \DB::getDefaultConnection();

        $owners = [
            'testOwner@mail.com' => [
                'firstName' => 'Test',
                'secondName' => 'Owner',
                'phone' => '9311111111',
            ],
        ];

        foreach ($owners as $email => $fields) {
            if (!Owner::where('email', '=', $email)->exists()) {
                $user = new Owner();
                $user->email = $email;
                $user->firstName = $fields['firstName'];
                $user->secondName = $fields['secondName'];
                $user->phone = $fields['phone'];
                try{
                    $user->setConnection($conn);
                    $user->save();
                }
                catch(Exception $e)
                {
                    dd($e->getMessage());
                }
            }
        }
    }
}
