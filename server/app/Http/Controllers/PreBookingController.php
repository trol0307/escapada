<?php

namespace App\DDD\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Cottage as Cottage;
use App\Models\Owner as Owner;
use App\Models\PreBooking as PreBooking;
use App\Models\Traveler as Traveler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class PreBookingController extends Controller
{
    public function store(Request $request) {

        $travelerFirstName = $request->input('traveler_firstname');
        $travelerLasttName = $request->input('traveler_lastname');
        $travelerEmail = $request->input('traveler_email');
        $travelerPhone = $request->input('traveler_phone');
        $cottageId = $request->input('cottage_id');
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');
        $prebookingType = $request->input('prebooking_type');
        $people_number = $request->input('people_number');

        try{
            $cottage = Cottage::findOrFail($cottageId);
        } catch(ModelNotFoundException $e) {

            return response ([
                'error' => [
                  'message' => 'Cottage not found',
                  'status_code' => 400
                  ],
              ],
            400 );
        }

        $traveller = new Traveler();
        $traveller->firstname = $travelerFirstName;
        $traveller->lastname = $travelerLasttName;
        $traveller->email = $travelerEmail;
        $traveller->phone = $travelerPhone;

        try{
            $traveller->save();
        } catch(Exception $e) {

            return response ([
                'error' => [
                  'message' => 'Traveler save process error',
                  'status_code' => 400
                  ],
              ],
            400 );
        }

        $travelerSavedId = $traveller->id;

        $preBooking = new PreBooking();
        $preBooking->type_id = $prebookingType;
        $preBooking->cottage_id = $cottage->id;
        $preBooking->start_date = $startDate;
        $preBooking->end_date = $endDate;
        $preBooking->people_number = $people_number;
        $preBooking->traveler_id = $travelerSavedId;

        try{
            $preBooking->save();
        }
        catch(Exception $e) {

            return response ([
                'error' => [
                  'message' => 'PreBooking save process error',
                  'status_code' => 400
                  ],
              ],
            400 );
        }

        return response ([
            'status' => 'ok',
            'message' => 'Prebooking saved'
          ],
        200 );
    }
}
