<?php

namespace App\Console\Commands\Cottage;

use Exception;
use Illuminate\Console\Command;
use App\Models\Cottage as Cottage;

class ListCottage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cottage:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Llista de Cottages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $headers = ['id', 'nom', 'adressa', 'poble', 'telèfon', 'nom propietari', 'creat'];
        $cottages = Cottage::all();
        $rows = [];
        foreach($cottages as $cottage) {
          $row = [];
          $row[] = $cottage->id;
          $row[] = $cottage->name;
          $row[] = $cottage->address;
          $row[] = $cottage->village;
          $row[] = $cottage->phone;
          $row[] = $cottage->owner->firstName . ' ' . $cottage->owner->secondName;
          $row[] = $cottage->created_at;
          $rows[] = $row;

        }

        $this->table($headers, $rows);
    }
}
