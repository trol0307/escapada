<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_booking', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cottage_id');
            $table->integer('type_id');
            $table->integer('traveler_id');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('people_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_booking');
    }
}
