<?php

namespace Tests\Unit\PreBooking;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Tests\Unit\BaseTest;


class PostTest extends BaseTest
{
  /**
     * setUp description
     * @return void
     */
    public function setUp()
    {
        parent::setup();
        $this->url = '/api/prebooking';
        $this->dataPath = base_path('tests/Requests');
    }

    /**
     * test status 400
     * @return void
     */
    public function testStatusBadRequest()
    {
        $contentString = file_get_contents("$this->dataPath/PreBookingError.json");
        $data = json_decode($contentString, true);

        // var_dump($contentString);
        // var_dump(json_decode($contentString));
        // var_dump(json_last_error());
        // var_dump(json_last_error_msg());

        $this
            ->post($this->url(), $data)
            ->assertStatus(400)
            ->assertJsonStructure(
                \Tests\Data\Error::STRUCTURE
            );
    }

    /**
     * test status 200
     * @return void
     */
    public function testStatusOk()
    {
        $contentString = file_get_contents("$this->dataPath/PreBookingOk.json");
        $data = json_decode($contentString, true);
        $this
            ->post($this->url(), $data)
            ->assertStatus(200)
            ->assertJsonStructure(
                \Tests\Data\PreBooking::STRUCTURE
            );
    }
}
