<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreBooking extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pre_booking';

    protected $primaryKey = 'id';

    /**
     * Get cottage for this prebooking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cottage()
    {
        return $this->belongsTo(Cottage::class, 'cottage_id', 'id');
    }

    /**
     * Get PreBookingType for this prebooking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function preBookingType()
    {
        return $this->belongsTo(PreBookingType::class, 'type_id', 'id');
    }

    /**
     * Get Traveler for this prebooking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function traveler()
    {
        return $this->belongsTo(Traveler::class, 'traveler_id', 'id');
    }
}
