<?php

namespace Tests\Unit;

use Tests\TestCase;

abstract class BaseTest extends TestCase
{
    protected $url = '/';

    /**
     * [url description]
     * @param  string $path
     * @return string
     */
    public function url(string $path = '')
    {
        return "{$this->url}/{$path}";
    }

}
