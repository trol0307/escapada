# Prova Senior Developer

Aquesta prova està basada en una aplicació feta amb Laravel 5.6, dockeritzada amd PHP 7.2, MySQL 5.7 i NGINX. Per executar-la caldrà doncs seguir les següents instruccions, i una vegada arrencats els contenidors, executar les comandes indicades per executar les comandes Artisan de Laravel dintre dels contenidors.
Per qualsevol dubte, siusplau poseu-vos en contacte amb mi. Gràcies


## Instruccions a seguir per instalar el projecte:

```
- $ git clone https://gitlab.com/trol0307/escapada.git
- $ sudo chown -R $USER:$USER escapada
- $ cd escapada
- $ docker-compose up -d
- $ docker-compose exec php-fpm composer update
- $ docker-compose kill
- $ ./arrencar_contenidors
- http://127.0.0.1:8000 per veure si l'aplicació ha arrencat correctament
- $ ./executar_composer.sh dump-autoload
- $ ./executar_artisan.sh migrate
- $ ./executar_artisan.sh db:seed
```
(Recordeu que en tractar-se d'un entorn dockeritzat de prova, si atureu els contenidors tambés es perdran les dades enregistrades a la base de dades MySQL)
## Comandes Artisan

### Cottages

#### all
Per veure tots els Cottages creats.

```
* ./executar_artisan.sh cottage:all
```

#### create
Per crear un nou Cottage.

```
* ./executar_artisan.sh cottage:create
```

### Owners

#### all
Per veure tots els Owners creats.

```
* ./executar_artisan.sh owner:all
```

#### create
Per crear un nou Owner.

```
* ./executar_artisan.sh owner:create
```

### PreBookingTypes

#### all
Per veure tots els PreBookingTypes creats.

```
* ./executar_artisan.sh prebooking_type:all
```

### Travelers

#### all
Per veure tots els Travelers creats.

```
* ./executar_artisan.sh traveler:all
```

### PreBookings

#### all
Per veure tots els PreBookings creats.

```
* ./executar_artisan.sh prebooking:all
```

## Com provar el funcionament

A l'arrel del projecte es pot trobar la col·lecció Postman per provar el funcionament de l'aplicació a l'arxiu : escapada.postman_collection.json

Importeu-lo a Postman i definiu les següents variables d'entorn:

- {{URL}} = 127.0.0.1:8000/api

A partir d'aquí podreu utilitzar les diferents crides per provar el funcionament, per exemple amb:
test_prebooking, podreu comprobar que s'afegeix un nou traveller i un nou prebooking a l'aplicació.

Utilitzeu les comandes artisan subministrades per comprobar-ho:

```
* ./executar_artisan.sh traveler:all
* ./executar_artisan.sh prebooking:all

```

## Executar els Tests Unitaris

A l'arrel del projecte es pot trobar un executable que ens permetrà executar els tests dintre del contenidor

```
* ./executar_test.sh

```
