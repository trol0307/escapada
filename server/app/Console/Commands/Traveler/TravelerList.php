<?php

namespace App\Console\Commands\Traveler;

use Illuminate\Console\Command;
use App\Models\Traveler as Traveler;

class TravelerList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'traveler:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Llista de travelers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $headers = ['id', 'nom', 'cognom', 'email', 'phone'];
        $traveler = Traveler::all(['id', 'firstName', 'lastName', 'email', 'phone'])->toArray();
        $this->table($headers, $traveler);
    }
}
