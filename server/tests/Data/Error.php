<?php

namespace Tests\Data;

class Error
{
    const STRUCTURE = array(
        'error' => [
            'message',
            'status_code',
        ],
    );
}
