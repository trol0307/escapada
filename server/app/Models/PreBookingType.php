<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreBookingType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pre_booking_type';

    protected $primaryKey = 'id';
}
