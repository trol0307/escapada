<?php

namespace Tests\Unit\Test;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Tests\Unit\BaseTest;


class PostTest extends BaseTest
{
  /**
     * setUp description
     * @return void
     */
    public function setUp()
    {
        parent::setup();

        $this->url = '/api/test';
    }


    /**
     * test status 200
     * @return void
     */
    public function testStatusOk()
    {
        $this
            ->get($this->url())
            ->assertStatus(200);
    }
}
