<?php

use Illuminate\Database\Seeder;
use App\Models\PreBookingType as PreBookingType;

class PreBookingTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conn = \DB::getDefaultConnection();

        $types = [
            'Estándar' => [
              'points' => 5,
            ],
            'Única' => [
              'points' => 15,
            ],
            'Cualificada' => [
              'points' => 35,
            ],
          ];

        foreach ($types as $name => $fields) {
            if (!PreBookingType::where('name', '=', $name)->exists()) {
                $user = new PreBookingType();
                $user->name = $name;
                $user->points = $fields['points'];
                $user->setConnection($conn);
                $user->save();
            }
        }
    }
}
