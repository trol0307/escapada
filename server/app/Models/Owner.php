<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cottage as Cottage;

class Owner extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'owner';

    protected $primaryKey = 'id';

    /**
     * Get the Cottages for this Owner
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cottages()
    {
        return $this->hasMany(Cottage::class, 'owner_id', 'id');
    }
}
