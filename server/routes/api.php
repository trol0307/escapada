<?php

use Illuminate\Http\Request;

use App\DDD\Controllers\TestController;
use App\DDD\Controllers\PreBookingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Test
Route::get('test', 'TestController@index');
Route::post('test/wrong', 'TestController@wrong');

//PreBooking
Route::post('prebooking', 'PreBookingController@store');
