<?php

namespace Tests\Unit\Test;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Tests\Unit\BaseTest;


class GetTest extends BaseTest
{
  /**
     * setUp description
     * @return void
     */
    public function setUp()
    {
        parent::setup();

        $this->url = '/api/test';
    }

    /**
     * test status 400
     * @return void
     */
    public function testStatusBadRequest()
    {
        $path = 'wrong';
        $this
            ->post($this->url($path))
            ->assertStatus(400)
            ->assertJsonStructure(
                \Tests\Data\Error::STRUCTURE
            );
    }

}
