<?php

namespace App\DDD\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class TestController extends Controller
{
    public function index(){
        return response([
          'status' => 'ok',
        ],200);
    }

    public function wrong(){
        return response ([
            'error' => [
              'message' => 'PreBooking save process error',
              'status_code' => 400
              ],
          ],
        400 );
    }
}
