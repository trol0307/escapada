<?php

use Illuminate\Database\Seeder;
use App\Models\User as User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conn = \DB::getDefaultConnection();

        $users = [
            'test@testmail.com' => [
              'name' => 'testUser',
              'password' => bcrypt('Google@123'),
            ],
          ];

        foreach ($users as $email => $fields) {

            if (!User::where('email', '=', $email)->exists()) {
              $user = new User();
              $user->email = $email;
              $user->name = $fields['name'];
              $user->password = $fields['password'];
              $user->setConnection($conn);
              $user->save();
            }
        }
    }
}
