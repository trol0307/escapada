<?php

namespace App\Console\Commands\PreBookingType;

use Illuminate\Console\Command;
use App\Models\PreBookingType as PreBookingType;

class PreBookingTypeList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prebooking_type:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Llista de tipus de pre-reserves';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $headers = ['id', 'nom', 'punts'];
        $prebookingType = PreBookingType::all(['id', 'name', 'points'])->toArray();
        $this->table($headers, $prebookingType);
    }
}
