#!/usr/bin/env bash

ENV_FILE=server/.env

# create .env file if it not already existing
if [ ! -f $ENV_FILE ]; then
  cp server/.env.example server/.env
fi

sudo chown -R $USER:www-data server/storage
sudo chown -R $USER:www-data server/bootstrap/cache

sudo chmod -R 775 server/storage
sudo chmod -R 775 server/bootstrap/cache

./aturar_contenidors.sh

docker-compose up -d
