<?php

namespace App\Console\Commands\Owner;

use Illuminate\Console\Command;
use App\Models\Owner as Owner;

class CreateOwner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'owner:create  {--firstName=} {--secondName=} {--email=} {--phone=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crear un nou propietari';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $firstName = $this->option('firstName');
        $secondName = $this->option('secondName');
        $email = $this->option('email');
        $phone = $this->option('phone');

        if (!$firstName) {
            $firstName = $this->ask('Quin és el seu nom?');
        }

        if (!$secondName) {
            $secondName = $this->ask('i el seu cognom?');
        }

        if (!$email) {
            $email = $this->ask('El seu email');
        }

        if (!$phone) {
            $phone = $this->ask('El seu telèfon de contacte');
        }

        $owner = new Owner();
        $owner->firstName = $firstName;
        $owner->secondName = $secondName;
        $owner->email = $email;
        $owner->phone = $phone;
        $owner->save();

        $this->info('Nou propietari creat.');
    }
}
