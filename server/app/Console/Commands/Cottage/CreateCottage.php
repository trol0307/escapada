<?php

namespace App\Console\Commands\Cottage;

use Exception;
use Illuminate\Console\Command;
use App\Models\Cottage as Cottage;

class CreateCottage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cottage:create {--name=} {--address=} {--phone=} {--village=} {--owner_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crear un nou Cottage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->option('name');
        $address = $this->option('address');
        $phone = $this->option('phone');
        $village = $this->option('village');
        $ownerId = $this->option('owner_id');

        if (!$name) {
            $name = $this->ask('Quin és el nom del cottage?');
        }

        if (!$address) {
            $address = $this->ask('i l\'adreça?');
        }

        if (!$phone) {
            $phone = $this->ask('el seu telèfon?');
        }

        if (!$village) {
            $village = $this->ask('el poble on es troba?');
        }

        if (!$ownerId) {
            $ownerId = $this->ask('l\'id del seu propietari?');
        }

        $cottage = new Cottage();
        $cottage->name = $name;
        $cottage->address = $address;
        $cottage->phone = $phone;
        $cottage->village = $village;
        $cottage->owner_id = $ownerId;
        try{
            $cottage->save();
            $this->info('Nou cottage creat.');
        }
        catch(Exception $e)
        {

            dd('Error en processar la tasca:'.$e->getMessage());
        }


    }
}
