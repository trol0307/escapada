<?php

namespace App\Console\Commands\Owner;

use Illuminate\Console\Command;
use App\Models\Owner as Owner;

class ListOwners extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'owner:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Llista de propietaris';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $headers = ['id', 'nom', 'cognom', 'correu electrònic', 'telèfon', 'creat'];
      $users = Owner::all(['id', 'firstName', 'secondName', 'email', 'phone', 'created_at'])->toArray();
      $this->table($headers, $users);
    }
}
