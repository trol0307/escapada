<?php

use Illuminate\Database\Seeder;
use App\Models\Cottage as Cottage;

class CottagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conn = \DB::getDefaultConnection();

        $cottages = [
            'Test Cottage' => [
              'address' => 'c/ Test Cottage Street n.1',
              'phone' => '9311111112',
              'village' => 'Test Village',
              'owner_id' => 1,
            ],
          ];

        foreach ($cottages as $name => $fields) {
            if (!Cottage::where('name', '=', $name)->exists()) {
                $cottage = new Cottage();
                $cottage->name = $name;
                $cottage->address = $fields['address'];
                $cottage->phone = $fields['phone'];
                $cottage->village = $fields['village'];
                $cottage->owner_id = $fields['owner_id'];
                try{
                    $cottage->setConnection($conn);
                    $cottage->save();
                }
                catch(Exception $e)
                {
                    dd($e->getMessage());
                }
            }
        }
    }
}
